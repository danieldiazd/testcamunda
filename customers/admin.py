from django.contrib import admin

# Export models
from .models import Customer

# Register your models here.
admin.site.register(Customer)