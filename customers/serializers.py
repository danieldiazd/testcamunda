from rest_framework import serializers
from .models import Customer

class TaskSerializer(serializers.ModelSerializer):
  class Meta:
    model = Customer # nombre de la tabla
    fields = '__all__'